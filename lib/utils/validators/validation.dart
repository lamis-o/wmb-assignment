class TValidator{
  static String? validateEmail(String? value){
    if(value == null || value.isEmpty){
      return 'Email is Required';
    }

    // Regular expression for email validation
    final emailRegExp = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
    if(!emailRegExp.hasMatch(value)){
      return 'Invalid email address';
    }
    return null;
  }
  //------------------------------------------------------------
  // validate password
  static String? validatePassword(String? value){
    if(value == null || value.isEmpty){
      return 'Password is Required';
    }

    // check for minimum password length
    if(value.length < 6){
      return 'Password must be at least 6 characters long';
    }
    return null;
  }

}