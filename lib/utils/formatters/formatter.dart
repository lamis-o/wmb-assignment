import 'package:intl/intl.dart';

class TFormatters{
  static String formatDate(DateTime? date){
    date ??= DateTime.now();
    return DateFormat('yyyy-mm-dd').format(date);
  }

  static String formatCurrency(double amount){
    return NumberFormat.currency(locale: 'en-US', symbol: '\$').format(amount);
  }
}