import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:music_site/utils/contstants/sizes.dart';
import 'package:music_site/utils/helpers/hepler_functions.dart';

class SignupScreen extends StatelessWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(TSizes.defaultSpace),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// title
              Text("Create a new account", style: Theme.of(context).textTheme.headlineMedium),
              const SizedBox(height: TSizes.spaceBtwSections),

              /// form
              Form(child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          expands: false,
                          decoration: const InputDecoration(
                            labelText: "First Name",
                            prefixIcon: Icon(Iconsax.user),
                          ),
                        ),
                      ),
                      const SizedBox(width: TSizes.spaceBtwInputFields),
                      Expanded(
                        child: TextFormField(
                          expands: false,
                          decoration: const InputDecoration(
                            labelText: "Last Name",
                            prefixIcon: Icon(Iconsax.user),
                          ),
                        ),
                      ),
                      TextFormField(),
                    ],
                  ),
                  const SizedBox(height: TSizes.spaceBtwInputFields),
                  /// username
                  TextFormField(
                    expands: false,
                    decoration: const InputDecoration(
                      labelText: "Username",
                      prefixIcon: Icon(Iconsax.user_edit),
                    ),
                  ),

                  /// Email
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: "E-mail",
                      prefixIcon: Icon(Iconsax.direct),
                    ),
                  ),
                  const SizedBox(height: TSizes.spaceBtwInputFields),

                  /// Address
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Address",
                      prefixIcon: Icon(Iconsax.location),
                    ),
                  ),
                  const SizedBox(height: TSizes.spaceBtwInputFields),

                  /// Password
                  TextFormField(
                    obscureText: true,
                    decoration: const InputDecoration(
                      labelText: "Password",
                      prefixIcon: Icon(Iconsax.password_check),
                      suffixIcon: Icon(Iconsax.eye_slash),
                    ),
                  ),
                  const SizedBox(height: TSizes.spaceBtwSections),

                  /// Sign up Button
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(onPressed: (){}, child: const Text("Create Account")),
                  ),
                ],
              ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
