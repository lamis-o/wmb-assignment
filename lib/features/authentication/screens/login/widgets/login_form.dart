import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:music_site/features/authentication/screens/signup/signup.dart';
import 'package:music_site/navigation_menu.dart';
import 'package:music_site/utils/contstants/sizes.dart';
import 'package:iconsax/iconsax.dart';

class TLoginForm extends StatelessWidget {
  const TLoginForm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(child: Padding(
      padding: const EdgeInsets.symmetric(vertical: TSizes.spaceBtwSections),
      child: Column(
        children: [
          /// username
          TextFormField(
            decoration: const InputDecoration(
              prefixIcon: Icon(Iconsax.direct_right),
              labelText: "username",
            ),
          ),
          const SizedBox(height: TSizes.spaceBtwInputFields),
          /// password
          TextFormField(
            decoration: const InputDecoration(
              prefixIcon: Icon(Iconsax.password_check),
              labelText: "password",
              suffixIcon: Icon(Iconsax.eye_slash),
            ),
          ),
          const SizedBox(height: TSizes.spaceBtwInputFields/2),
          /// remember me & forget password
          Row(
            children: [
              /// Remember me
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Checkbox(value: true, onChanged: (value){}),
                  const Text("Remember me"),
                ],
              ),
              /// Forget password
              TextButton(onPressed: (){}, child: const Text("forgot your password?")),
              const SizedBox(height: TSizes.spaceBtwSections),

              /// sign in button
              SizedBox(width: double.infinity, child: ElevatedButton(onPressed: ()=> Get.to(()=> const NavigationMenu()), child: Text("Sign in"))),
              /// create account button
              SizedBox(width: double.infinity, child: OutlinedButton(onPressed: () => Get.to(()=> const SignupScreen()), child: Text("Create an account"))),
            ],
          )
        ],
      ),
    ),
    );
  }
}