import 'package:flutter/material.dart';
import 'package:music_site/common/styles/spacing_styles.dart';
import 'package:music_site/features/authentication/screens/login/widgets/login_form.dart';
import 'package:music_site/features/authentication/screens/login/widgets/login_header.dart';
import 'package:music_site/utils/helpers/hepler_functions.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dark = THelperFunctions.isDarkMode(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: TSpacingStyle.paddingWithAppBarHeight,
          child: Column(
            children: [
              /// logo, title & subtitle
              TLoginHeader(),
              /// Form
              TLoginForm(),
            ],
          ),
        ),
      ),
    );
  }
}




